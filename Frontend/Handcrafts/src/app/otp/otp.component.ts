import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css'],
})
export class OtpComponent implements OnInit {
  otp: any;
  formModel: any = {};
  error: string = '';
  customer: any;

  constructor(
    private service: CustomerService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {}

  otpSubmit(otpForm: any) {
    this.otp = localStorage.getItem('otp');
    console.log(this.otp);
    if (otpForm.otp == this.otp) {
      console.log('Submitted');
      this.toastr.success('success OTP');
      this.router.navigate(['login']);
    } else {
      this.toastr.error('Invalid OTP');
    }

    (error: any) => {
      console.error('Error fetching data:', error);
    };
  }
}
