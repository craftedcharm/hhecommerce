import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';
import { OtpComponent } from './otp/otp.component';
import { DescriptionComponent } from './description/description.component';
import { AdminComponent } from './admin/admin.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AdminproductsComponent } from './adminproducts/adminproducts.component';
import { AdmincategoriesComponent } from './admincategories/admincategories.component';
import { CategoryComponent } from './category/category.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { CartComponent } from './cart/cart.component';
import { Products2Component } from './products2/products2.component';
import { PasswordComponent } from './password/password.component';
import { Products3Component } from './products3/products3.component';
import { AdminlistComponent } from './adminlist/adminlist.component';
import { AdminordersComponent } from './adminorders/adminorders.component';
import { PaymentComponent } from './payment/payment.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '', component: HomeComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'otp', component: OtpComponent },
  { path: 'adminProducts', component: AdminproductsComponent },
  { path: 'adminCategories', component: AdmincategoriesComponent },
  { path: 'adminCustomer', component: AdminComponent },
  { path: 'otp', component: OtpComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'admin', component: AdminComponent },
  { path: '', component: LoginComponent },
  { path: 'description', component: DescriptionComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'categories', component: CategoryComponent },
  { path: 'search', component: SearchResultsComponent },
  { path: 'cart', component: CartComponent },
  { path: 'products2', component: Products2Component },
  { path: 'password', component: PasswordComponent },
  { path: 'products3', component: Products3Component },
  { path: 'admincategories', component: AdmincategoriesComponent },
  { path: 'adminlist', component: AdminlistComponent },
  { path: 'adminproducts', component: AdminproductsComponent },
  { path: 'adminorders', component: AdminordersComponent },
  { path: 'payment', component: PaymentComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
