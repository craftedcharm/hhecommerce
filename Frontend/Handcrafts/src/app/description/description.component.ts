import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrl: './description.component.css',
})
export class DescriptionComponent implements OnInit {
  selectedProduct: any;
  products: any;
  emailId: any;
  cartProducts: any;
  constructor(private router: Router) {}

  ngOnInit() {
    const storedProduct = localStorage.getItem('selectedProduct');
    this.selectedProduct = storedProduct ? JSON.parse(storedProduct) : null;
    console.log(this.selectedProduct);
  }

  generateStars(rating: number): string {
    const fullStars = Math.floor(rating);
    const halfStar = rating % 1 !== 0;
    const emptyStars = 5 - fullStars - (halfStar ? 1 : 0);
    const stars =
      '★'.repeat(fullStars) + (halfStar ? '½' : '') + '☆'.repeat(emptyStars);
    return stars;
  }

  calculateDiscountedPrice(price: number, discount: number): number {
    const discountAmount = (price * discount) / 100;
    return price - discountAmount;
  }

  addToCart(selectedProduct: any) {
    this.cartProducts = this.cartProducts || [];
    console.log('Adding to cart:', selectedProduct);
    this.cartProducts.push(selectedProduct);
    localStorage.setItem('cartItems', JSON.stringify(this.cartProducts));
    console.log('Cart after adding:', this.cartProducts);
  }
}
