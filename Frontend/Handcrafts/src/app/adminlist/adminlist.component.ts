import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminlist',
  templateUrl: './adminlist.component.html',
  styleUrl: './adminlist.component.css',
})
export class AdminlistComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateToCategoriesList() {
    this.router.navigate(['/admincategories']);
  }

  navigateToCustomerList() {
    this.router.navigate(['/admin']);
  }

  navigateToOrdersList() {
    this.router.navigate(['/adminorders']);
  }

  navigateToProductsList() {
    this.router.navigate(['/adminproducts']);
  }
}
