import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  emailAddress: any;
  pattern = '[a-zA-Z0-9._%+-]+@gmail.com';
  password: any;
  rememberMe: any;
  emailInvalid: any;
  passwordInvalid: any;
  customer: any;
  customers: any;

  constructor(
    private router: Router,
    private service: CustomerService,
    private toastr: ToastrService
  ) {
    this.customers = [];
  }

  ngOnInit() {}

  submit() {
    console.log('EmailId : ' + this.emailAddress);
    console.log('Password: ' + this.password);
  }

  async loginSubmit(loginForm: any) {
    console.log(loginForm);
    console.log(loginForm.emailAddress);
    console.log(loginForm.password);

    if (
      loginForm.emailAddress == 'ADMIN@gmail.com' &&
      loginForm.password == 'Admin@12'
    ) {
      this.service.setIsUserLoggedIn();
      localStorage.setItem('email', loginForm.emailAddress);
      this.router.navigate(['adminlist']);
    } else {
      this.customer = null;
      await this.service
        .customerLogin(loginForm.emailAddress, loginForm.password)
        .then((data: any) => {
          console.log(data);
          this.customer = data;
        });

      if (this.customer != null) {
        this.service.setIsUserLoggedIn();
        localStorage.setItem('email', loginForm.emailAddress);
        this.router.navigate(['categories']);
      } else {
        this.toastr.error('Invalid Credentials', 'Error', {
          closeButton: true,
          progressBar: true,
          positionClass: 'toast-top-right',
          tapToDismiss: false,
          timeOut: 3000,
        });
      }
    }
  }

  validateEmail(): void {
    this.emailInvalid = !this.emailAddress.includes('@gmail.com');
  }

  validatePassword(): void {
    this.passwordInvalid =
      !/(?=.[A-Z])(?=.[a-z])(?=.\d)(?=.[@$!%?&])[A-Za-z\d@$!%?&]{8,}/.test(
        this.password
      );
  }

  navigateToRegister() {
    this.router.navigate(['register']);
  }

  navigateToForgotPassword() {
    this.router.navigate(['forgot-password']); 
  }
}
