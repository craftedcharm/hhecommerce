import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

interface Order {
  name: string;
  email: string;
  phone: string;
  amount: number;
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  constructor(private http: HttpClient) {}

  createOrder(order: Order): Observable<any> {
    return this.http.post(
      'http://localhost:8085/pg/createOrder',
      {
        customerName: order.name,
        emailAddress: order.email,
        phoneNumber: order.phone,
        amount: order.amount,
      },
      httpOptions
    );
  }
}
