import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  isAdmin: boolean = false;
  isUserLoggedIn: boolean;
  cartItems: any;
  cart: any;

  private emailIdSource = new BehaviorSubject<string>('');
  currentEmailId = this.emailIdSource.asObservable();

  constructor(private http: HttpClient) {
    this.cartItems = [];
    this.isUserLoggedIn = false;
  }

  addToCart(product: any) {
    this.cartItems.push(product);
    console.log('Item added to cart:', product);
  }

  getCartItems() {
    return this.cartItems;
  }

  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }

  registerCustomer(customer: any): any {
    return this.http.post('http://localhost:8085/addCustomer', customer);
  }

  getAllCustomers(): any {
    return this.http.get('http://localhost:8085/getAllCustomers');
  }

  getAllOrders() {
    return this.http.get('http://localhost:8085/getAllOrders');
  }

  deleteOrderById(orderId: any) {
    return this.http.delete('http://localhost:8085/deleteOrder/' + orderId);
  }

  customerLogin(emailAddress: any, password: any): any {
    return this.http
      .get(`http://localhost:8085/customerLogin/${emailAddress}/${password}`)
      .toPromise();
  }
  deleteCustomerById(customerId: any): any {
    return this.http.delete(
      'http://localhost:8085/deleteCustomer/' + customerId
    );
  }

  getAllProducts(): any {
    return this.http.get('http://localhost:8085/getAllProducts');
  }

  updateProduct(product: any) {
    return this.http.put('http://localhost:8085/updateProduct', product);
  }

  deleteProductById(productId: any): any {
    return this.http.delete(
      'http://localhost:8085/deleteProductById/' + productId
    );
  }

  getAllCategories(): any {
    return this.http.get('http://localhost:8085/getCategories');
  }

  updateCategory(category: any) {
    return this.http.put('http://localhost:8085/updateCategory', category);
  }

  deleteCategoryById(categoryId: any): any {
    return this.http.delete(
      'http://localhost:8085/deleteCategory/' + categoryId
    );
  }

  changeEmailId(emailId: string) {
    this.emailIdSource.next(emailId);
  }

  getUserOtp(emailAddress: any): any {
    return this.http.get(
      'http://localhost:8085/getCustomerEmailId/' + emailAddress
    );
  }

  updateUserPassword(customer: any): any {
    return this.http.put(
      'http://localhost:8085/updateCustomerPassword',
      customer
    );
  }

  logout() {
    this.isAdmin = false;
    this.isUserLoggedIn = false;
  }
}
