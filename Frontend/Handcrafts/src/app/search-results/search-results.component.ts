import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css'],
})
export class SearchResultsComponent implements OnInit {
  searchQuery: string = '';

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      if (params && params['query']) {
        this.searchQuery = params['query'];
        this.performSearch(this.searchQuery);
      }
    });
  }

  performSearch(query: string) {
    console.log('Performing search for:', query);
  }
}
