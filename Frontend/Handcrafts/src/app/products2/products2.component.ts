import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-products2',
  templateUrl: './products2.component.html',
  styleUrl: './products2.component.css',
})
export class Products2Component implements OnInit {
  products: any[];
  emailId: any;
  cartItems: any;
  selectedProduct: any;
  customerService: any;

  constructor(private router: Router, private service: CustomerService) {
    this.emailId = localStorage.getItem('emailId');
    this.cartItems = [];

    this.products = [
      {
        pName: 'FlowerVase',
        price: 499.0,
        discount: 10,
        description: 'Flowers Make Everything Better.',
        details:
          'Solid Wood Carved brown Flowervase, Antique carving, Free Stand Design for maximum stability with accurate thickness. ',
        imgsrc: 'assets/Flowervase.jpg',
        rating: 4.5,
      },
      {
        pName: 'Homedecor',
        price: 550.0,
        discount: 12,
        description: 'Revamp Your Home with Our Unique Pieces',
        details:
          'Multicolour Art decors with people palying drums theme. Antique home decor and beautifully decorated showpieces.',
        imgsrc: 'assets/Homedecor.jpg',
        rating: 4.0,
      },
      {
        pName: 'Jute',
        price: 400.0,
        discount: 15,
        description: 'The natural choice for a sustainable future.',
        details:
          'Handmade Jute or natural leather horse toys made with reinforced layers of natural products. .',
        imgsrc: 'assets/Jute.jpg',
        rating: 3.5,
      },
      {
        pName: 'Fish',
        price: 399.0,
        discount: 10,
        description: 'From Wood to Wonders.',
        details: 'Handmade natuarl brown coloured wooden flexible fish toy.',
        imgsrc: 'assets/Fish.jpg',
        rating: 4.0,
      },
      {
        pName: 'Birds',
        price: 419.0,
        discount: 13,
        description: 'Creating Narratives with Timber.',
        details:
          'Wooden traditional style Yellow coloured birds, which enhance the look.',
        imgsrc: 'assets/Birds.jpg',
        rating: 4.0,
      },
      {
        pName: 'Bicycle',
        price: 299.0,
        discount: 11,
        description: 'Bringing Nature’s Beauty to Your Home',
        details:
          'Black and Brown coloured bicycle antique showpiece, Made to look like a reclaimed vintage find.',
        imgsrc: 'assets/bicycle.jpg',
        rating: 4.5,
      },
    ];
  }

  calculateDiscountedPrice(price: number, discount: number): number {
    const discountAmount = (price * discount) / 100;
    return price - discountAmount;
  }

  generateStars(rating: number): string {
    const fullStars = Math.floor(rating);
    const halfStar = rating % 1 !== 0;
    const emptyStars = 5 - fullStars - (halfStar ? 1 : 0);
    const stars =
      '★'.repeat(fullStars) + (halfStar ? '½' : '') + '☆'.repeat(emptyStars);
    return stars;
  }

  ngOnInit() {}

  selectProduct(product: any) {
    this.selectedProduct = product;
  }

  addToCart(product: any) {
    this.service.addToCart(product);
  }

  onClickProduct(product: any): void {
    localStorage.setItem('selectedProduct', JSON.stringify(product));
    this.router.navigate(['description']);
  }
}
