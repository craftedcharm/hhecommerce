import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';

declare var jQuery: any;

@Component({
  selector: 'app-admincategories',
  templateUrl: './admincategories.component.html',
  styleUrls: ['./admincategories.component.css'],
})

export class AdmincategoriesComponent implements OnInit {
  categories: any;
  editCat: any;

  constructor(private service: CustomerService, private toastr: ToastrService) {
    this.editCat = {
      categoryId: '',
      categoryName: '',
    };
  }

  ngOnInit() {
    this.service.getAllCategories().subscribe((data: any) => {
      this.categories = data;
    });
  }

  editCategory(category: any) {
    console.log(category);
    this.editCat = category;
    jQuery('#myModal').modal('show');
  }

  updateCategory() {
    console.log(this.editCat);
    this.service.updateCategory(this.editCat).subscribe((data: any) => {
      console.log(data);
    });
  }

  deleteCategory(category: any) {
    this.service
      .deleteCategoryById(category.categoryId)
      .subscribe((data: any) => {
        console.log(data);
      });
    const i = this.categories.findIndex((element: any) => {
      return element.categoryId == category.categoryId;
    });

    this.categories.splice(i, 1);
    this.toastr.success('Successfull', 'Deleted', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000,
    });
  }
}
