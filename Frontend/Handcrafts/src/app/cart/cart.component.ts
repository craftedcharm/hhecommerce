import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css',
})
export class CartComponent implements OnInit {
  emailId: any;
  cartItems: any;
  cartProducts: any;

  constructor(private service: CustomerService, private router: Router) {
    this.cartItems = service.getCartItems();
  }

  ngOnInit() {}

  onPurchase() {
    localStorage.setItem('amount', this.calculateTotal());
    this.router.navigate(['/payment']);
  }

  onDelete(index: number) {
    this.cartItems.splice(index, 1);
    this.calculateTotal();
  }

  calculateTotal(): string {
    return this.cartItems.reduce(
      (total: any, product: any) => total + (product.price || 0),
      0
    );
  }
}
