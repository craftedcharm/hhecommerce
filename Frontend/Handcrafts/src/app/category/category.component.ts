import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrl: './category.component.css',
})
export class CategoryComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateToProducts() {
    this.router.navigate(['/products']);
  }

  navigateToProducts2() {
    this.router.navigate(['/products2']);
  }

  navigateToProducts3() {
    this.router.navigate(['/products3']);
  }
}
