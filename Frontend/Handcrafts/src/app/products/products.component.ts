import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css',
})
export class ProductsComponent implements OnInit {
  products: any[];
  emailId: any;
  cartItems: any;
  selectedProduct: any;
  customerService: any;

  constructor(
    private router: Router,
    private service: CustomerService,
    private cartService: CartService
  ) {
    this.emailId = localStorage.getItem('emailId');
    this.cartItems = [];

    this.products = [
      {
        pName: 'Bracelet',
        price: 399.0,
        discount: 10,
        description: 'Feel the Sparkle of Beads',
        details:
          'Blue beads with 4.9inch, Which is of type chain and with no Gemstone made with Alloy.',
        imgsrc: 'assets/Bracelet.jpg',
        rating: 4.5,
      },
      {
        pName: 'Thread Bangles',
        price: 560.0,
        discount: 10,
        description: 'Designed to Dazzle',
        details:
          'Thread bangles made with Silk thread, Gemtype-Pearl, No-Metal type.',
        imgsrc: 'assets/bangles.jpg',
        rating: 4.0,
      },
      {
        pName: 'Cotton Dress',
        price: 2400.0,
        discount: 10,
        description: 'Cotton: Comfort That Lasts.',
        details:
          'Yellow and Ash Cotton Dress Material with super comfort material composition.',
        imgsrc: 'assets/Cotton.jpg',
        rating: 3.5,
      },
      {
        pName: 'Earrings',
        price: 699.0,
        discount: 10,
        description: 'Keep that ears shining through',
        details: 'Earrings with no metal type, No Gemstone and lightweight',
        imgsrc: 'assets/Earrings.jpg',
        rating: 4.0,
      },
      {
        pName: 'Neckpiece',
        price: 429.0,
        discount: 10,
        description: 'Rare Jewels, Timeless Elegance',
        details:
          'Metal type Neckset made with beads and silver pendant, lightweight.',
        imgsrc: 'assets/Jewellry.jpg',
        rating: 3.0,
      },
      {
        pName: 'Silk Sarees',
        price: 1099.0,
        discount: 10,
        description: 'Flaunt your Style with Natural Silk',
        details: 'Thraed weave saress with Embroidere with zari.',
        imgsrc: 'assets/Silk.jpg',
        rating: 3.5,
      },
    ];
  }

  calculateDiscountedPrice(price: number, discount: number): number {
    const discountAmount = (price * discount) / 100;
    return price - discountAmount;
  }

  generateStars(rating: number): string {
    const fullStars = Math.floor(rating);
    const halfStar = rating % 1 !== 0;
    const emptyStars = 5 - fullStars - (halfStar ? 1 : 0);
    const stars =
      '★'.repeat(fullStars) + (halfStar ? '½' : '') + '☆'.repeat(emptyStars);
    return stars;
  }

  ngOnInit() {}

  selectProduct(product: any) {
    this.selectedProduct = product;
  }

  addToCart(product: any) {
    this.service.addToCart(product);
  }

  onClickProduct(product: any): void {
    localStorage.setItem('selectedProduct', JSON.stringify(product));
    this.router.navigate(['description']);
  }
}
