import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  customer: any;
  firstName: string = '';
  lastName: string = '';
  emailAddress: string = '';
  password: string = '';
  state: string = '';
  address: string = '';
  phoneNumber: string = '';
  confirmPassword: any;
  gender: string = '';
  registrationForm: FormGroup;
  generatedCaptcha: string = '';

  ngOnInit() {
    this.generateCaptcha();
  }

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private service: CustomerService,
    private toastr: ToastrService
  ) {
    this.registrationForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      emailAddress: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(30),
          Validators.pattern('[a-zA-Z0-9._%+-]+@gmail.com$'),
        ],
      ],
      gender: ['', Validators.required],
    });
    this.generateCaptcha();
    this.customer = {
      firstName: '',
      lastName: '',
      emailAddress: '',
      password: '',
      state: '',
      address: '',
      gender: '',
      phoneNumber: '',
    };
  }

  generateCaptcha() {
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+';
    const captchaLength = 6;

    let captcha = '';
    for (let i = 0; i < captchaLength; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      captcha += characters.charAt(randomIndex);
    }
    this.generatedCaptcha = captcha;
  }

  validateCaptcha(control: {
    value: string;
  }): { [key: string]: boolean } | null {
    if (control.value !== this.generatedCaptcha) {
      return { incorrect: true };
    }
    return null;
  }

  isGmailDomainInvalid(email: string): boolean {
    if (email && email.toLowerCase().includes('@gmail.com')) {
      return false;
    }
    return true;
  }

  isValidPhoneNumber(phoneNumber: string): boolean {
    if (/^[6-9]\d{9}$/.test(phoneNumber)) {
      return true;
    }
    return false;
  }

  submit() {
    console.log('First Name: ' + this.firstName);
    console.log('Last Name: ' + this.lastName);
    console.log('Email-Id: ' + this.emailAddress);
    console.log('Password: ' + this.password);
    console.log('Country: ' + this.state);
    console.log('Address: ' + this.address);
    console.log('Gender: ' + this.gender);
    console.log('Phone Number: ' + this.phoneNumber);
  }

  registerSubmit(regForm: any) {
    console.log('Register form submitted:', regForm);
    this.customer.firstName = regForm.firstName;
    this.customer.lastName = regForm.lastName;
    this.customer.emailAddress = regForm.emailAddress;
    this.customer.password = regForm.password;
    this.customer.state = regForm.state;
    this.customer.address = regForm.address;
    this.customer.gender = regForm.gender;
    this.customer.phoneNumber = regForm.phoneNumber;

    console.log(this.customer);

    if (
      regForm.address != '' &&
      regForm.firstName != '' &&
      regForm.password != '' &&
      regForm.PhoneNumber != '' &&
      regForm.lastName != '' &&
      regForm.emailAddress != '' &&
      regForm.state != '' &&
      regForm.gender != ''
    ) {
      this.service.registerCustomer(this.customer).subscribe((data: any) => {
        console.log(data);
        localStorage.setItem('otp', data.otp);
      });

      this.router.navigate(['otp']);
    } else {
      this.toastr.error('Please provide required Details', 'Error', {
        closeButton: true,
        progressBar: true,
        positionClass: 'toast-top-right',
        tapToDismiss: false,
        timeOut: 3000,
      });
      this.router.navigate(['register']);
    }
  }
}
