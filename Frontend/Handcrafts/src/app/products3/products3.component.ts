import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products3',
  templateUrl: './products3.component.html',
  styleUrl: './products3.component.css',
})
export class Products3Component implements OnInit {
  products: any[];
  emailId: any;
  cartItems: any;
  selectedProduct: any;
  customerService: any;

  constructor(private router: Router, private service: CustomerService) {
    this.emailId = localStorage.getItem('emailId');
    this.cartItems = [];

    this.products = [
      {
        pName: 'Kettle',
        price: 399.0,
        discount: 11,
        description: 'Experience the joy of pottery.',
        details:
          'A set of ceramic teapot and jar with unique style and colours decorated with leves and flowers in blue.',
        imgsrc: 'assets/Kettles.jpg',
        rating: 4.0,
      },
      {
        pName: 'Pots',
        price: 550.0,
        discount: 12,
        description: 'Handcrafted pottery for your home.',
        details:
          'Set of handmade ceramic pots with lids for baking 4 items with unique styles.',
        imgsrc: 'assets/Ceramic3.jpg',
        rating: 4.0,
      },
      {
        pName: 'Cup',
        price: 299.0,
        discount: 13,
        description: 'Functional art for your everyday life.',
        details:
          'Handmade Ceramic hot or cold beverage cup with Dripping Glazes and unique colours.',
        imgsrc: 'assets/Cup.jpg',
        rating: 4.5,
      },
      {
        pName: 'Plates',
        price: 399.0,
        discount: 14,
        description: 'Functional pottery that adds style to your life.',
        details:
          'A set of 3 handmade and hand painted Ceramic plates, with imprints of Flowers,Leaves and Patterns.',
        imgsrc: 'assets/Plates.jpg',
        rating: 4.0,
      },
      {
        pName: 'Bowls',
        price: 689.0,
        discount: 10,
        description: 'Transform your kitchen with our pottery.',
        details:
          'Ceramic Soup Bowl with handle, Pea and Leaf Motif, Handmade bowl.',
        imgsrc: 'assets/Bowls.jpg',
        rating: 4.0,
      },
      {
        pName: 'Baking Trays',
        price: 799.0,
        discount: 15,
        description: 'The perfect material for your home.',
        details:
          'Handmade Rectangular Cake baking ceramic Casserole tray, Stoneware with multiple colours.',
        imgsrc: 'assets/Trays.jpg',
        rating: 4.5,
      },
    ];
  }

  calculateDiscountedPrice(price: number, discount: number): number {
    const discountAmount = (price * discount) / 100;
    return price - discountAmount;
  }

  generateStars(rating: number): string {
    const fullStars = Math.floor(rating);
    const halfStar = rating % 1 !== 0;
    const emptyStars = 5 - fullStars - (halfStar ? 1 : 0);

    const stars =
      '★'.repeat(fullStars) + (halfStar ? '½' : '') + '☆'.repeat(emptyStars);

    return stars;
  }

  ngOnInit() {}

  selectProduct(product: any) {
    this.selectedProduct = product;
  }

  addToCart(product: any) {
    this.service.addToCart(product);
  }

  onClickProduct(product: any): void {
    localStorage.setItem('selectedProduct', JSON.stringify(product));
    this.router.navigate(['description']);
  }
}
