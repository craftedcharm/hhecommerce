import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ToastrModule } from 'ngx-toastr';
import { AboutusComponent } from './aboutus/aboutus.component';
import { FooterComponent } from './footer/footer.component';
import { LogoutComponent } from './logout/logout.component';
import { HttpClientModule } from '@angular/common/http';
import { AdminComponent } from './admin/admin.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { OtpComponent } from './otp/otp.component';
import { HeaderComponent } from './header/header.component';
import { AdminproductsComponent } from './adminproducts/adminproducts.component';
import { AdmincategoriesComponent } from './admincategories/admincategories.component';
import { DescriptionComponent } from './description/description.component';
import { GenderPipe } from './gender.pipe';
import { SearchResultsComponent } from './search-results/search-results.component';
import { CartComponent } from './cart/cart.component';
import { Products2Component } from './products2/products2.component';
import { PasswordComponent } from './password/password.component';
import { CommonModule } from '@angular/common';
import { Products3Component } from './products3/products3.component';
import { AdminordersComponent } from './adminorders/adminorders.component';
import { AdminlistComponent } from './adminlist/adminlist.component';
import { PaymentComponent } from './payment/payment.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutusComponent,
    ProductsComponent,
    LogoutComponent,
    AdminComponent,
    DescriptionComponent,
    GenderPipe,
    OtpComponent,
    ForgotPasswordComponent,
    SearchResultsComponent,
    CartComponent,
    AdminproductsComponent,
    AdmincategoriesComponent,
    Products2Component,
    PasswordComponent,
    Products3Component,
    AdmincategoriesComponent,
    AdminlistComponent,
    AdminordersComponent,
    PaymentComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CommonModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
