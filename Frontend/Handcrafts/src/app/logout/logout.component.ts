import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css',
})
export class LogoutComponent {
  constructor(private router: Router, private toastr: ToastrService) {
    localStorage.removeItem('emailId');
    localStorage.clear();

    this.toastr.success('logout Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000,
    });

    this.router.navigate(['login']);
  }

  ngOnInit() {}
}
