import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
})
export class ForgotPasswordComponent implements OnInit {
  otp1: any;
  formModel: any = {};
  error: string = '';

  constructor(
    private service: CustomerService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {}

  getOtp() {
    this.service.getUserOtp(this.formModel.emailAddress).subscribe(
      (data: any) => {
        this.otp1 = data.otp;
        console.log(this.otp1);
      },
      (error: any) => {
        console.error('Error fetching workers:', error);
      }
    );

    this.service.changeEmailId(this.formModel.emailAddress);
  }

  loginSubmit(loginForm: any) {
    if (loginForm.emailAddress == this.otp1) {
      console.log('Submitted');
      this.router.navigate(['password']);
    } else {
      this.toastr.error('Invalid Credentials');
    }
  }
}
