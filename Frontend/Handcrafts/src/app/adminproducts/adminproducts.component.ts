import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';

declare var jQuery: any;

@Component({
  selector: 'app-adminproducts',
  templateUrl: './adminproducts.component.html',
  styleUrls: ['./adminproducts.component.css'],
})
export class AdminproductsComponent implements OnInit {
  products: any;
  categories: any;
  editPro: any;

  constructor(private service: CustomerService, private toastr: ToastrService) {
    this.editPro = {
      productId: '',
      productName: '',
      price: '',
      quantityAvailable: '',
      category: {
        categoryId: '',
      },
    };
  }

  ngOnInit() {
    this.service.getAllProducts().subscribe((data: any) => {
      this.products = data;
    });
    this.service.getAllCategories().subscribe((data: any) => {
      this.categories = data;
    });
  }

  editProduct(product: any) {
    console.log(product);
    this.editPro = product;
    jQuery('#myModal').modal('show');
  }

  updateProduct() {
    console.log(this.editPro);
    this.service.updateProduct(this.editPro).subscribe((data: any) => {
      console.log(data);
    });
  }

  deleteProduct(product: any) {
    this.service.deleteProductById(product.productId).subscribe((data: any) => {
      console.log(data);
    });
    const i = this.products.findIndex((element: any) => {
      return element.productId == product.productId;
    });

    this.products.splice(i, 1);

    this.toastr.success('Successfull', 'Deleted', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000, // 3 seconds
    });
  }
}
