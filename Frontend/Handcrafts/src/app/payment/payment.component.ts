import { HttpClient } from '@angular/common/http';
import { Component, HostListener } from '@angular/core';
import { OrderService } from '../order.service';
import { Router } from '@angular/router';

declare var Razorpay: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css'],
})
export class PaymentComponent {
  title = 'demo';

  form: any = {
    amount: localStorage.getItem('amount'),
  };

  constructor(
    private http: HttpClient,
    private orderService: OrderService,
    private router: Router
  ) {}

  ngOnInit() {}

  sayHello() {
    alert('Hello DK');
  }

  paymentId: string = '';
  error: string = '';

  options = {
    key: '',
    amount: '',
    name: 'Crafted Charm',
    description: 'Web Development',
    image:
      'https://tse1.mm.bing.net/th?id=OIP.6WnpIDMlSxi5CWCzS4yC3AAAAA&pid=Api&P=0&h=180',
    order_id: '',
    handler: function (response: any) {
      var event = new CustomEvent('payment.success', {
        detail: response,
        bubbles: true,
        cancelable: true,
      });
      window.dispatchEvent(event);
    },
    prefill: {
      name: '',
      email: '',
      contact: '',
    },
    notes: {
      address: '',
    },
    theme: {
      color: '#3399cc',
    },
  };

  onSubmit(): void {
    this.paymentId = '';
    this.error = '';
    this.orderService.createOrder(this.form).subscribe(
      (data) => {
        this.options.key = data.secretId;
        this.options.order_id = data.razorpayOrderId;
        this.options.amount = data.applicationFee; //paise
        this.options.prefill.name = 'CraftedCharm';
        this.options.prefill.email = 'craftedcharm@gmail.com';
        this.options.prefill.contact = '999999999';

        if (data.pgName === 'razor2') {
          this.options.image = '';
          var rzp1 = new Razorpay(this.options);
          rzp1.open();
        } else {
          var rzp2 = new Razorpay(this.options);
          rzp2.open();
        }

        rzp1.on('payment.failed', (response: any) => {
          console.log(response);
          console.log(response.error.code);
          console.log(response.error.description);
          console.log(response.error.source);
          console.log(response.error.step);
          console.log(response.error.reason);
          console.log(response.error.metadata.order_id);
          console.log(response.error.metadata.payment_id);
          this.error = response.error.reason;
        });
      },
      (err) => {
        this.error = err.error.message;
      }
    );
  }

  @HostListener('window:payment.success', ['$event'])
  onPaymentSuccess(event: any): void {
    console.log(event.detail);
    this.router.navigate(['/']);
  }
}
