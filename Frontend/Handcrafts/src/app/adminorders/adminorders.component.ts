import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-adminorders',
  templateUrl: './adminorders.component.html',
  styleUrl: './adminorders.component.css',
})
export class AdminordersComponent implements OnInit {
  orders: any;
  emailId: any;

  constructor(private service: CustomerService, private toastr: ToastrService) {
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.service.getAllOrders().subscribe((data: any) => { this.orders = data; });
  }

  deleteOrder(ord: any) {
    this.service.deleteOrderById(ord.orderId).subscribe((data: any) => {
      console.log(data);
    });
    const i = this.orders.findIndex((element: any) => {
      return element.orderId == ord.orderId;
    });

    this.orders.splice(i, 1);
    this.toastr.error('Successfull', 'Deleted', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000,
    });
  }
}
