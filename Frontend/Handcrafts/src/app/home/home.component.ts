import {
  Component,
  AfterViewInit,
  ElementRef,
  QueryList,
  ViewChildren,
} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements AfterViewInit {
  @ViewChildren('scroller')
  scrollers!: QueryList<ElementRef>;

  constructor() {}

  ngAfterViewInit(): void {
    this.addAnimation();
  }

  addAnimation(): void {
    if (this.scrollers) {
      this.scrollers.forEach((scroller) => {
        scroller.nativeElement.setAttribute('data-animated', true);
        
        const scrollerInner =
          scroller.nativeElement.querySelector('.scroller_inner');
        const scrollerInnerContent: Element[] = Array.from(
          scrollerInner.children
        );

        scrollerInnerContent.forEach((item: Element) => {
          const duplicateItem = item.cloneNode(true) as Element;
          duplicateItem.setAttribute('aria-hidden', 'true');
          scrollerInner.appendChild(duplicateItem);
        });
      });
    }
  }
}
