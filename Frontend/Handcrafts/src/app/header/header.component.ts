import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  cartItems: any;
  searchQuery: string = '';
  isLoggedIn: boolean = false;
  
  constructor(public customerService: CustomerService, private router: Router) {
    this.cartItems = customerService.getCartItems();
  }

  ngOnInit(): void {}

  logout() {
    this.customerService.logout();
    this.router.navigate(['']);
  }

  onSubmit() {
    if (this.searchQuery) {
      this.router.navigate(['/categories'], {
        queryParams: { query: this.searchQuery },
      });
    }
  }
}
