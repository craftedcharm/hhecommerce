import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrl: './admin.component.css',
})

export class AdminComponent implements OnInit {
  customers: any;
  emailId: any;

  constructor(private service: CustomerService, private toastr: ToastrService) {
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.service.getAllCustomers().subscribe((data: any) => {
      this.customers = data;
    });
  }

  deleteCustomer(cust: any) {
    this.service.deleteCustomerById(cust.customerId).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.customers.findIndex((element: any) => {
      return element.customerId == cust.customerId;
    });

    this.customers.splice(i, 1);
    this.toastr.error('Successfull', 'Deleted', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000,
    });
  }
}
