import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrl: './password.component.css',
})
export class PasswordComponent {
  emailAddress: string = '';
  customer: any;

  constructor(
    private service: CustomerService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.customer = {
      emailAddress: '',
      password: '',
    };
  }

  ngOnInit() {
    this.service.currentEmailId.subscribe((emailAddress) => {
      this.emailAddress = emailAddress;
    });
  }

  passwordChanged(regForm: any) {
    this.customer.emailAddress = regForm.emailAddress;
    this.customer.password = regForm.password;

    console.log(this.customer.emailAddress);
    console.log(this.customer.password);

    this.service.updateUserPassword(this.customer).subscribe((data: any) => {
      console.log(data);
    });
    this.toastr.success('password changed');
    this.router.navigate(['login']);
  }
}
