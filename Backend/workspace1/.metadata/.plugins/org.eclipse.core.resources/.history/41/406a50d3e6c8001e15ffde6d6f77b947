package com.dao;

import java.util.List;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.model.Customer;


@Service
public class CustomerDao {

    // Injecting CustomerRepository for database operations
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
	private JavaMailSender mailSender;

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Customer getCustomerById(int customerId) {
        return customerRepository.findById(customerId).orElse(null);
    }
    
    public List<Customer> getCustomerByName(String firstName) {
        return customerRepository.findByCustomerName(firstName);
    }

    public Customer updateCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public void deleteCustomer(int customerId) {
        customerRepository.deleteById(customerId);
    }

    public Customer addCustomer(Customer customer) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String encryptedPwd = bcrypt.encode(customer.getPassword());
        customer.setPassword(encryptedPwd);
        Customer savedCustomer = customerRepository.save(customer);
        sendWelcomeEmail(savedCustomer);
        return savedCustomer;
    }
    
     private void sendWelcomeEmail(Customer customer) {
		
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(customer.getEmailAddress());
		message.setSubject("Welcome to CraftedCharm – Unwrap the Artistry of Handlooms and Handicrafts!");
		message.setText("Dear " + customer.getFirstName() + ",\n\n"
				+ "Greetings from CraftedCharm! We are thrilled to welcome you as a "
				+ "valued member of our handlooms and handicrafts community. " + ",\n\n"
				+"At CraftedCharm,"
				+ "we take pride in curating a unique collection of exquisite handcrafted products "
				+ "that showcase the rich heritage of artisanal craftsmanship. "
				+ "As a registered member,"
				+ "you now have exclusive access to a world of artistry and tradition.");

		mailSender.send(message);
	  }

    public Customer updateCustomerPassword(Customer customer) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String encryptedPwd = bcrypt.encode(customer.getPassword());
        customerRepository.updatePassword(customer.getCustomerId(), encryptedPwd);
        customer.setPassword(encryptedPwd);
        return customer;
    }
    
	 public Customer customerLogin(String emailAddress, String password) {
	        Customer customer = customerRepository.findByEmailId(emailAddress);
	        
	        if (customer != null) {
	            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
	            if (bcrypt.matches(password, customer.getPassword())) {
	                return customer;
	            }
	        }
	        
	        return null;
	    }


   
}

